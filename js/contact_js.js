// Code used for reference: 
// https://www.freecodecamp.org/news/form-validation-with-html5-and-javascript/
// https://embed.plnkr.co/plunk/8ujYdL1BxZftGoS4Cf14

const myForm = document.querySelector('#my-form');
myForm.addEventListener('submit', validate);

const nameError = document.getElementById("nblankError");
const emError = document.getElementById("emblankError");

function validate(e) {
    e.preventDefault();
    valblank(e);
    allLetter(e);
    valemail(e);
    valph(e);
}

// Function to validate if fields are blank
function valblank(e) {
    e.preventDefault();
    const NameField = document.getElementById("fname");
    const emField = document.getElementById("email");   
    const phField = document.getElementById("phone");
    const msgField = document.getElementById("subject");
    
  let valid = true;
//Validate if "Name" field blank
  if (!NameField.value) {
    const nameError = document.getElementById("nblankError");
    nameError.classList.add("visible");
    NameField.classList.add("invalid");
    nameError.setAttribute("aria-hidden", false);
    nameError.setAttribute("aria-invalid", true);
  }
  else {
    const nameError = document.getElementById("nblankError");
    nameError.classList.remove("visible");
    NameField.classList.remove("invalid");
    nameError.setAttribute("aria-hidden", true);
    nameError.setAttribute("aria-invalid", false);
  }

  //Validate if "Email" field blank
  if (!emField.value) {
    const emError = document.getElementById("emblankError");
    emError.classList.add("visible");
    emField.classList.add("invalid");
    emError.setAttribute("aria-hidden", false);
    emError.setAttribute("aria-invalid", true);
  }
  else {
    const emError = document.getElementById("emblankError");
    emError.classList.remove("visible");
    emField.classList.remove("invalid");
    emError.setAttribute("aria-hidden", true);
    emError.setAttribute("aria-invalid", false);
  }

  //Validate if "Phone Number" field blank
  if (!phField.value) {
    const phError = document.getElementById("phblankError");
    phError.classList.add("visible");
    phField.classList.add("invalid");
    phError.setAttribute("aria-hidden", false);
    phError.setAttribute("aria-invalid", true);
  }
  else {
    const phError = document.getElementById("phblankError");
    phError.classList.remove("visible");
    phField.classList.remove("invalid");
    phError.setAttribute("aria-hidden", true);
    phError.setAttribute("aria-invalid", false);
  }
  //Validate if "Message" field blank
  if (!msgField.value) {
    const msgError = document.getElementById("msgblankError");
    msgError.classList.add("visible");
    msgField.classList.add("invalid");
    msgError.setAttribute("aria-hidden", false);
    msgError.setAttribute("aria-invalid", true);
  }
  else {
    const msgError = document.getElementById("msgblankError");
    msgError.classList.remove("visible");
    msgField.classList.remove("invalid");
    msgError.setAttribute("aria-hidden", true);
    msgError.setAttribute("aria-invalid", false);
  }
  return valid;
}

// Function to validate if name has letters only
function allLetter(e) {
    e.preventDefault();

    const alphNameField = document.getElementById("fname");
    const alphNameError = document.getElementById("nvalError");

    var letters = /[A-Za-z]+$/;
    let valid = true;
// If letters are present, error message will stay hidden
	if (alphNameField.value.match(letters)) {
        alphNameError.classList.remove("visible");
        alphNameField.classList.remove("invalid");
        alphNameError.setAttribute("aria-hidden", true);
        alphNameError.setAttribute("aria-invalid", false);
      }
// Else if there is no field entered, it will run the valblank function
    else if (!alphNameField.value) {
        valblank(e)
    }
// Otherwise if there are no letters then the error message will show
    else   {
        alphNameError.classList.add("visible");
        alphNameField.classList.add("invalid");
        alphNameError.setAttribute("aria-hidden", false);
        alphNameError.setAttribute("aria-invalid", true);
        }
        
    return valid;
}

// Function to validate email
function valemail(e) {
    e.preventDefault();

    const valEmField = document.getElementById("email");
    const valEmError = document.getElementById("emValError");

    var mailformat = /\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let valid = true;

	if (valEmField.value.match(mailformat)) {
        valEmError.classList.remove("visible");
        valEmField.classList.remove("invalid");
        valEmError.setAttribute("aria-hidden", true);
        valEmError.setAttribute("aria-invalid", false);
      }

      else if (!valEmField.value) {
        valblank(e)
    }

    else   {
        valEmError.classList.add("visible");
        valEmField.classList.add("invalid");
        valEmError.setAttribute("aria-hidden", false);
        valEmError.setAttribute("aria-invalid", true);
        }
        
    return valid;
}

// Function to validate phone number
function valph(e) {
    e.preventDefault();

    const valPhField = document.getElementById("phone");
    const valPhError = document.getElementById("phValError");

    var phoneno = /^\d{10}$/;
    let valid = true;

    if (valPhField.value.match(phoneno)) {
        valPhError.classList.remove("visible");
        valPhField.classList.remove("invalid");
        valPhError.setAttribute("aria-hidden", true);
        valPhError.setAttribute("aria-invalid", false);
      }

    else if (!valPhField.value) {
        valblank(e)
    }

    else   {
        valPhError.classList.add("visible");
        valPhField.classList.add("invalid");
        valPhError.setAttribute("aria-hidden", false);
        valPhError.setAttribute("aria-invalid", true);
        }
        
    return valid;
}

